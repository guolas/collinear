/*************************************************************************
 * Name:
 * Email:
 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Comparator;

public class Point implements Comparable<Point> {

   // compare points by slope
   public final Comparator<Point> SLOPE_ORDER = new BySlope();

   /* Comparator inner class to compare by slope */
   private class BySlope implements Comparator<Point> {
      public int compare(Point pointA, Point pointB) {
         int comparison = 0;
         double slopeA = slopeTo(pointA);
         double slopeB = slopeTo(pointB);
         if (slopeA < slopeB)      comparison = -1;
         else if (slopeA > slopeB) comparison = +1;
         else                      comparison = 0;
         return comparison;
      }
   }

   private final int x;                              // x coordinate
   private final int y;                              // y coordinate

   // create the point (x, y)
   public Point(int x, int y) {
      /* DO NOT MODIFY */
      this.x = x;
      this.y = y;
   }

   // plot this point to standard drawing
   public void draw() {
      /* DO NOT MODIFY */
      StdDraw.point(x, y);
   }

   // draw line between this point and that point to standard drawing
   public void drawTo(Point that) {
      /* DO NOT MODIFY */
      StdDraw.line(this.x, this.y, that.x, that.y);
   }

   // slope between this point and that point
   public double slopeTo(Point that) {
      double slope = Double.NEGATIVE_INFINITY; 
      int comparison = this.compareTo(that);
      if (comparison == -1)
         slope = ((double) (that.y - this.y)) / ((double) (that.x - this.x));
      else if (comparison == 1)
         slope = ((double) (this.y - that.y)) / ((double) (this.x - that.x));
      return slope;
   }

   // is this point lexicographically smaller than that one?
   // comparing y-coordinates and breaking ties by x-coordinates
   public int compareTo(Point that) {
      int comparison = 0;
      if (this.y < that.y) comparison = -1;
      else if (this.y > that.y) comparison = +1;
      else if (this.x < that.x) comparison = -1;
      else if (this.x > that.x) comparison = +1;
      return comparison;
   }

   // return string representation of this point
   public String toString() {
      /* DO NOT MODIFY */
      return "(" + x + ", " + y + ")";
   }

   // unit test
   public static void main(String[] args) {
      Point p1 = new Point(3, 2);
      Point p2 = new Point(2, 0);
      Point p3 = new Point(3, 2);
      Point p4 = new Point(1, 3);
      Point p5 = new Point(10, 2);
      Point p6 = new Point(-10, 2);

      StdOut.println("-----------------");
      StdOut.println("Testing compareTo");

      StdOut.println(p1 + "<=>" + p2);
      StdOut.println(p1.compareTo(p2));
      assert p1.compareTo(p2) == 1;

      StdOut.println(p1 + "<=>" + p3);
      StdOut.println(p1.compareTo(p3));
      assert p1.compareTo(p3) == 0;

      StdOut.println(p1 + "<=>" + p4);
      StdOut.println(p1.compareTo(p4));
      assert p1.compareTo(p4) == -1;

      StdOut.println("---------------");
      StdOut.println("Testing slopeTo");

      StdOut.println(p1 + "<=>" + p2);
      StdOut.println(p1.slopeTo(p2));
      assert p1.slopeTo(p2) == 2.0;


      StdOut.println(p1 + "<=>" + p3);
      StdOut.println(p1.slopeTo(p3));
      assert p1.slopeTo(p3) == Double.NEGATIVE_INFINITY;

      StdOut.println(p1 + "<=>" + p5);
      StdOut.println(p1.slopeTo(p5));
      assert new Double(p1.slopeTo(p5)).equals(new Double(0.0));

      StdOut.println("--------");
      StdOut.println(p1.SLOPE_ORDER.compare(p3, p6));

      StdOut.println(p1.SLOPE_ORDER.compare(p1, p3));
   }
}
