import java.util.Arrays;

public class Brute {
   public static void main(String[] args) {
      In inputFile = new In(args[0]);

      /* Resize the drawing canvas */
      StdDraw.setXscale(0, 32768);
      StdDraw.setYscale(0, 32768);

      /* Get the number of points */
      int numberOfPoints = inputFile.readInt();
      /* Initialize the array to store the points */
      Point[] points = new Point[numberOfPoints];
      /* Get each point's coordinates */
      for (int i = 0; i < numberOfPoints; i++) {
         int xCoordinate = inputFile.readInt();
         int yCoordinate = inputFile.readInt();
         points[i] = new Point(xCoordinate, yCoordinate);
         points[i].draw();
      }

      Point[] result = new Point[4];

      /* search for 4-tuples that are in line */
      for (int i = 0; i < numberOfPoints; i++) {
         for (int j = i + 1; j < numberOfPoints; j++) { 
            double slope2 = points[i].slopeTo(points[j]);
            for (int k = j + 1; k < numberOfPoints; k++) {
               double slope3 = points[i].slopeTo(points[k]);
               if (slope2 == slope3) {
                  for (int l = k + 1; l < numberOfPoints; l++) {
                     double slope4 = points[i].slopeTo(points[l]);
                     if (slope3 == slope4) {
                        result[0] = points[i];
                        result[1] = points[j];
                        result[2] = points[k];
                        result[3] = points[l];
                        Arrays.sort(result);
                        result[0].drawTo(result[3]);
                        StdOut.print(result[0] + " -> ");
                        StdOut.print(result[1] + " -> ");
                        StdOut.print(result[2] + " -> ");
                        StdOut.println(result[3]);
                     } 
                  }
               }
            }
         }
      }
   }
}
