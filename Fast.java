import java.util.Arrays;

public class Fast {
   public static void main(String[] args) {
      In inputFile = new In(args[0]);

      /* Get the number of points */
      int numberOfPoints = inputFile.readInt();

      /* Resize the drawing canvas */
      StdDraw.setXscale(0, 32768);
      StdDraw.setYscale(0, 32768);

      /* Initialize the array to store the points */
      Point[] points = new Point[numberOfPoints];
      Point[] tempPoints = new Point[numberOfPoints];
      /* Get each point's coordinates */
      for (int i = 0; i < numberOfPoints; i++) {
         int xCoordinate = inputFile.readInt();
         int yCoordinate = inputFile.readInt();
         points[i] = new Point(xCoordinate, yCoordinate);
         points[i].draw();
      }

      int counter = 1;
      /* iterate through all the input Points */
      for (int i = 0; i < numberOfPoints; i++) {
         /* All points except the current one ordered by the slope formed
            with the first one, points[i]*/
         Arrays.sort(points, i + 1, numberOfPoints, points[i].SLOPE_ORDER);
         counter = 1;
         double previousSlope = 0;
         double currentSlope = 0;
         for (int j = i + 1; j < numberOfPoints; j++) {
            if (counter == 1) {
               previousSlope = points[i].slopeTo(points[j]);
               counter++;
            } else {
               currentSlope = points[i].slopeTo(points[j]);
               if (currentSlope != previousSlope) {
                  if (counter >= 4)
                     printLine(points, i, j - 1, counter);
                  previousSlope = currentSlope;
                  counter = 2;
               } else {
                  counter++;
               }
            }
         }
         if (counter >= 4)
            printLine(points, i, numberOfPoints - 1, counter);
      }
   }

   private static void printLine(Point[] points, int i, int j, int counter) {
      Point[] result = new Point[counter];
      result[0] = points[i];
      for (int k = 0; k < counter - 1; k++) {
         result[k + 1] = points[j - k];
      }
      Arrays.sort(result);
      result[0].drawTo(result[counter - 1]);
      StdOut.print(result[0]);
      for (int k = 1; k < counter; k++) {
         StdOut.print(" -> " + result[k]);
      }
      StdOut.println("");
   }
}
